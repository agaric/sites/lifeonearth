---
# Suggested settings to adjust:
# vagrant_hostname
# vagrant_machine_name
# drupal_site_name
# theme (set if you are building from sass)
# See ../box/default.config.yml for additional settings
# which can be overridden here.

#vagrant_box: geerlingguy/drupal-vm # is reported to be faster.
vagrant_box: geerlingguy/debian9

# These variables are no longer honored. In order to use them specifically,
# revert the changes to use vconfig values in <project_root>/box/Vagrant
# And disable the pre-tasks
# vagrant_hostname: drutopia.local
# vagrant_machine_name: drutopia-com-vm

# You can specify an IP, or else the vagrant-auto_network will set it for you:
vagrant_ip: 0.0.0.0

vagrant_synced_folders:
# A list of synced folders, with the keys 'local_path', 'destination', and
# a 'type' of [nfs|rsync|smb] (leave empty for slow native shares). See
# http://docs.drupalvm.com/en/latest/getting-started/syncing-folders/ for more.
  # The first synced folder will be used for the default Drupal installation, if
  # any of the build_* settings are 'true'. By default the folder is set to
  # the drupal-vm folder.
  - local_path: .
    destination: /var/www/drutopia
    type: nfs
    create: true

# Ensure vagrant plugins are installed.
vagrant_plugins:
  - name: vagrant-vbguest
  - name: vagrant-hostsupdater
  - name: vagrant-auto_network

# Setting theme to empty will skip sass step:
# (drutopia's default theme, octavia, is included)
theme:

# Set this to 'false' if you are using a different site deployment strategy and
# would like to configure 'vagrant_synced_folders' and 'apache_vhosts' manually.
drupal_build_makefile: false
drush_makefile_path: "{{ config_dir }}/drupal.make.yml"
drush_make_options: "--no-gitinfofile"

# Set 'build_makefile' to 'false' and this to 'true' if you are using a
# composer based site deployment strategy.
drupal_build_composer: false
drupal_composer_path: false #"{{ config_dir }}/drupal.composer.json"
drupal_composer_install_dir: "/var/www/drutopia/"
drupal_composer_dependencies: []

# Set this to 'true' and 'build_makefile', 'build_composer' to 'false' if you
# are using Composer's create-project as a site deployment strategy.
drupal_build_composer_project: false
drupal_composer_project_package: "drupal-composer/drupal-project:8.x-dev"
drupal_composer_project_options: "--prefer-dist --stability dev --no-interaction"

# Set this to 'false' if you don't need to install drupal (using the drupal_*
# settings below), but instead copy down a database (e.g. using drush sql-sync).
drupal_install_site: false

# Required Drupal settings.
drupal_core_path: "{{ drupal_composer_install_dir }}/web"
drupal_core_owner: "{{ drupalvm_user }}"
drupal_db_user: drupal
drupal_db_password: drupal
drupal_db_name: drupal
drupal_db_host: localhost

# Settings for installing a Drupal site if 'drupal_install_site:' is 'true'.
drupal_major_version: 8
drupal_domain: "{{ vagrant_hostname }}"
drupal_site_name: "Drutopia"
drupal_install_profile: drutopia
drupal_enable_modules: [ ]
drupal_account_name: admin
drupal_account_pass: admin
drupal_account_email: admin@drutopia.org

# Additional arguments or options to pass to `drush site-install`.
drupal_site_install_extra_args: []

# Cron jobs are added to the vagrant user's crontab. Keys include name
# (required), minute, hour, day, weekday, month, job (required), and state.
drupalvm_cron_jobs: []

# Drupal VM automatically creates a drush alias file in your ~/.drush folder if
# this variable is 'true'.
configure_drush_aliases: true
drush_aliases_host_template: "templates/drupalvm.aliases.drushrc.php.j2"
drush_aliases_guest_template: "templates/drupalvm-local.aliases.drushrc.php.j2"

# Apache VirtualHosts. Add one for each site you are running inside the VM. For
# multisite deployments, you can point multiple servernames at one documentroot.
# View the geerlingguy.apache Ansible Role README for more options.
apache_vhosts:
  - servername: "{{ drupal_domain }}"
    serveralias: "www.{{ drupal_domain }}"
    documentroot: "{{ drupal_core_path }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

  - servername: "adminer.{{ vagrant_hostname }}"
    documentroot: "{{ adminer_install_dir }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

  - servername: "xhprof.{{ vagrant_hostname }}"
    documentroot: "{{ php_xhprof_html_dir }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

  - servername: "pimpmylog.{{ vagrant_hostname }}"
    documentroot: "{{ pimpmylog_install_dir }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

  - servername: "{{ vagrant_ip }}"
    serveralias: "dashboard.{{ vagrant_hostname }}"
    documentroot: "{{ dashboard_install_dir }}"
    extra_parameters: "{{ apache_vhost_php_fpm_parameters }}"

apache_mods_enabled:
  - expires.load
  - headers.load
  - ssl.load
  - rewrite.load
  - proxy.load
  - proxy_fcgi.load

# MySQL databases and users.
mysql_databases:
  - name: "{{ drupal_db_name }}"
    encoding: utf8mb4
    collation: utf8mb4_general_ci

mysql_users:
  - name: "{{ drupal_db_user }}"
    host: "%"
    password: "{{ drupal_db_password }}"
    priv: "{{ drupal_db_name }}.*:ALL"

# Comment out any extra utilities you don't want to install. If you don't want
# to install *any* extras, set this value to an empty set, e.g. `[]`.
installed_extras:
  - adminer
  # - blackfire
  # - drupalconsole
  - drush
  # - elasticsearch
  # - java
  - mailhog
  # - memcached
  # - newrelic
  - nodejs
  - pimpmylog
  # - redis
  - ruby
  # - selenium
  # - solr
  # - tideways
  # - upload-progress
  - varnish
  - xdebug
  # - xhprof # use `tideways` if you're installing PHP 7+

# Add any extra apt or yum packages you would like installed.
extra_packages:
  - sassc
  - sqlite

# You can configure almost anything else on the server in the rest of this file.
drush_version: "8.1.14"

extra_security_enabled: false

firewall_enabled: true
firewall_allowed_tcp_ports:
  - "22"
  - "25"
  - "80"
  - "81"
  - "443"
  - "4444"
  - "8025"
  - "8080"
  - "8443"
  - "8983"
  - "9200"
firewall_log_dropped_packets: false
firewall_disable_firewalld: true
firewall_disable_ufw: true

# PHP Configuration. Currently-supported versions: 5.6, 7.0, 7.1, 7.2.
# See version-specific notes: http://docs.drupalvm.com/en/latest/configurations/php/
php_version: "7.1"
php_install_recommends: no
php_memory_limit: "192M"
php_display_errors: "On"
php_display_startup_errors: "On"
php_realpath_cache_size: "1024K"
php_sendmail_path: "/opt/mailhog/mhsendmail"
php_opcache_enabled_in_ini: true
php_opcache_memory_consumption: "192"
php_opcache_max_accelerated_files: 4096
php_max_input_vars: "4000"
php_packages_extra: ['php7.1-zip']

# MySQL Configuration.
mysql_root_password: root
mysql_slow_query_log_enabled: true
mysql_slow_query_time: 2
mysql_wait_timeout: 300
adminer_install_filename: index.php

# Node.js configuration (if enabled above).
# Valid examples: "0.10", "0.12", "4.x", "5.x", "6.x".
nodejs_version: "8.x"
nodejs_npm_global_packages: []
nodejs_install_npm_user: "{{ drupalvm_user }}"
npm_config_prefix: "/home/{{ drupalvm_user }}/.npm-global"

# Ruby Configuration (if enabled above).
ruby_install_gems_user: "{{ drupalvm_user }}"
ruby_install_gems: ['rake']

# Varnish Configuration (if enabled above).
varnish_listen_port: "81"
varnish_default_vcl_template_path: templates/drupalvm.vcl.j2
varnish_default_backend_host: "127.0.0.1"
varnish_default_backend_port: "80"

# Pimp my Log settings.
pimpmylog_install_dir: /usr/share/php/pimpmylog
pimpmylog_grant_all_privs: true

# XDebug configuration. XDebug is disabled by default for better performance.
php_xdebug_default_enable: 1
php_xdebug_coverage_enable: 0
php_xdebug_cli_enable: 1
php_xdebug_remote_enable: 1
php_xdebug_remote_connect_back: 1
# Use PHPSTORM for PHPStorm, sublime.xdebug for Sublime Text.
php_xdebug_idekey: PHPSTORM
php_xdebug_max_nesting_level: 256
php_xdebug_remote_host: "{{ ansible_default_ipv4.gateway }}"

# Solr Configuration (if enabled above).
solr_version: "6.6.2"
solr_xms: "64M"
solr_xmx: "128M"

# Selenium configuration.
selenium_version: 2.53.0

# Docker configuration.
docker_container_name: drutopia-vm
docker_image_name: drutopia-vm
docker_image_path: ~/Downloads

# Other configuration.
dashboard_install_dir: /var/www/dashboard
known_hosts_path: ~/.ssh/known_hosts
hostname_configure: true
hostname_fqdn: "{{ vagrant_hostname }}"
ssh_home: "{{ drupal_core_path }}"

pre_provision_tasks_dir: ../../provisioning/box/pre-tasks/*.yml
post_provision_tasks_dir: ../../provisioning/box/post-tasks/*.yml
